package com.jjawor.hibernateTutorial;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

//import com.jjawor.databaseTypes.Events;
import com.jjawor.hibernateTutorial.util.HibernateUtil;

import java.util.*;

public class EventManager {

	public static void main(String[] args) {
		EventManager mgr = new EventManager();

		// if (args[0].equals("store")) {
		mgr.createAndStoreEvent("My Event", new Date());
		// }

		// HibernateUtil.getSessionFactory().close();
	}

	public void createAndStoreEvent(String title, Date theDate) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Event theEvent = new Event();
		theEvent.setTitle(title);
		theEvent.setDate(theDate);
		session.save(theEvent);

		session.getTransaction().commit();
	}

	public void createAndStoreEvent(String title, Date theDate,
			LocalSessionFactoryBean sesionFactory) {
		// new Configuration().configure()

		// System.out.println("a1");
		sesionFactory.getObject();
		// System.out.println("a22");
		Session session = sesionFactory.getObject().getCurrentSession();
		// System.out.println("a2");
		session.beginTransaction();

		// System.out.println("dffs");
		
		
		//Events theEvent = new Events();
		//theEvent.setTitle(title);
		//theEvent.setEventDate(theDate);
		//session.save(theEvent);

		session.getTransaction().commit();
		// session.close();
	}
}
