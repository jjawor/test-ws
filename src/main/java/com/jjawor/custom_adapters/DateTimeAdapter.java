package com.jjawor.custom_adapters;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DateTimeAdapter extends XmlAdapter<String, Date> {
	public Date unmarshal(String value) {
			return javax.xml.bind.DatatypeConverter.parseDateTime(value).getTime();
	}

	public String marshal(Date value) {
		Calendar cal=new GregorianCalendar();
		cal.setTime(value);
		return (javax.xml.bind.DatatypeConverter.printDateTime(cal));
	}
}
