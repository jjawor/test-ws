package com.jjawor.elm.ws;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.authorization.WSPermisionLevel;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.CreateUserRequest;
import com.jjawor.elm.schemas.CreateUserResponse;
import com.jjawor.elm.schemas.RequestType;
import com.jjawor.elm.schemas.ResultStatus;

@Endpoint
public class CreateUserEndpoint {

	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@Autowired
	private WSPermisionLevel permisionLevel;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "CreateUserRequest")
	public @ResponsePayload
	CreateUserResponse handleRequest(
			@RequestPayload CreateUserRequest request) throws Exception {
		CreateUserResponse res = new CreateUserResponse();
		Context context=sessionsHolderBean.getContext(request.getToken());
		res.setStatus(ResultStatus.FAILURE);
		if(!permisionLevel.checkPermisionLevel(context, this.getClass())){
			res.setStatus(ResultStatus.NO_SESSION);
			return res;
		}
		if(RequestType.CREATE.equals(request.getOperationType()))
		{
			int userId=localDAO.createUser(request.getUser());
			res.setStatus(ResultStatus.OK);
			res.setId(BigInteger.valueOf(userId));
		}
		else if(RequestType.DELETE.equals(request.getOperationType()))
		{
			localDAO.deleteUser(request.getUser());
			res.setStatus(ResultStatus.OK);
		}
		else if(RequestType.MODIFY.equals(request.getOperationType()))
		{
			localDAO.modifyUser(request.getUser());
			res.setStatus(ResultStatus.OK);
			res.setId(request.getUser().getId());
		}
		
		return res;
	}

}
