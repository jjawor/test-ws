package com.jjawor.elm.ws;

import java.math.BigInteger;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.authorization.WSPermisionLevel;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.GetUserListRequest;
import com.jjawor.elm.schemas.GetUserListResponse;
import com.jjawor.elm.schemas.ResultStatus;

@Endpoint
public class GetUserListEndpoint {
	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@Autowired
	private WSPermisionLevel permisionLevel;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetUserListRequest")
	public @ResponsePayload
	GetUserListResponse handleRequest(
			@RequestPayload GetUserListRequest request) throws Exception {
		GetUserListResponse res=new GetUserListResponse();
		Context context=sessionsHolderBean.getContext(request.getToken());
		if(!permisionLevel.checkPermisionLevel(context, this.getClass())){
			Logger.getLogger(this.getClass()).info("authenetication for"+request.getToken()+"failed");
			res.setStatus(ResultStatus.NO_SESSION);
			return res;
		}
		localDAO.getUserList(res.getUsers());
		res.setStatus(ResultStatus.OK);
		
		return res;
	}

}
