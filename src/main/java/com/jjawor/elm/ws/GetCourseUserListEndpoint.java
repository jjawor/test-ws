package com.jjawor.elm.ws;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.authorization.WSPermisionLevel;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.GetCourseUserListRequest;
import com.jjawor.elm.schemas.GetCourseUserListResponse;
import com.jjawor.elm.schemas.GetUserListRequest;
import com.jjawor.elm.schemas.GetUserListResponse;
import com.jjawor.elm.schemas.ResultStatus;

@Endpoint
public class GetCourseUserListEndpoint {
	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@Autowired
	private WSPermisionLevel permisionLevel;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetCourseUserListRequest")
	public @ResponsePayload
	GetCourseUserListResponse handleRequest(
			@RequestPayload GetCourseUserListRequest request) throws Exception {
		GetCourseUserListResponse res=new GetCourseUserListResponse();
		Context context=sessionsHolderBean.getContext(request.getToken());
		if(!permisionLevel.checkPermisionLevel(context, this.getClass())){
			res.setStatus(ResultStatus.NO_SESSION);
			return res;
		}
		localDAO.getCourseUserList(res.getCourses(),context);
		res.setStatus(ResultStatus.OK);
		
		return res;
	}

}
