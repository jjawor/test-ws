package com.jjawor.elm.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.ResultStatus;
import com.jjawor.elm.schemas.SignInRequest;
import com.jjawor.elm.schemas.SignInResponse;

@Endpoint
public class SignInEndpoint {
	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "SignInRequest")
	public @ResponsePayload
	SignInResponse handleRequest(
			@RequestPayload SignInRequest request) throws Exception {
		SignInResponse response=new SignInResponse();
		Context context=sessionsHolderBean.signIn(request.getLogin(), request.getPassword());
		if(context==null)response.setStatus(ResultStatus.FAILURE);
		else{
		response.setStatus(ResultStatus.OK);
		response.setToken(context.getToken());
		response.setUserType(context.getUser().getType());
		response.setId(context.getUser().getId());
		}
		return response;
	}

}
