package com.jjawor.elm.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.authorization.WSPermisionLevel;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.GetCategoryQuestionListRequest;
import com.jjawor.elm.schemas.GetCategoryQuestionListResponse;
import com.jjawor.elm.schemas.ResultStatus;

@Endpoint
public class GetCategoryQuestionListEndpoint {
	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@Autowired
	private WSPermisionLevel permisionLevel;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetCategoryQuestionListRequest")
	public @ResponsePayload
	GetCategoryQuestionListResponse handleRequest(
			@RequestPayload GetCategoryQuestionListRequest request) throws Exception {
		GetCategoryQuestionListResponse res=new GetCategoryQuestionListResponse();
		Context context=sessionsHolderBean.getContext(request.getToken());
		if(!permisionLevel.checkPermisionLevel(context, this.getClass())){
			res.setStatus(ResultStatus.NO_SESSION);
			return res;
		}
		localDAO.getCategoryQuestionList(res.getCategories(),context);
		res.setStatus(ResultStatus.OK);
		
		return res;
	}

}
