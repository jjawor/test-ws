package com.jjawor.elm.ws;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.authorization.WSPermisionLevel;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.CreateTestRequest;
import com.jjawor.elm.schemas.CreateTestResponse;
import com.jjawor.elm.schemas.RequestType;
import com.jjawor.elm.schemas.ResultStatus;

@Endpoint
public class CreateTestEndpoint {

	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@Autowired
	private WSPermisionLevel permisionLevel;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "CreateTestRequest")
	public @ResponsePayload
	CreateTestResponse handleRequest(
			@RequestPayload CreateTestRequest request) throws Exception {
		CreateTestResponse res = new CreateTestResponse();
		Context context=sessionsHolderBean.getContext(request.getToken());
		if(!permisionLevel.checkPermisionLevel(context, this.getClass())){
			res.setStatus(ResultStatus.NO_SESSION);
			return res;
		}
		if(RequestType.CREATE.equals(request.getOperationType()))
		{
			int id=localDAO.createTest(request.getTest(), context);
			res.setStatus(ResultStatus.OK);
			res.setId(BigInteger.valueOf(id));
		}
		else if(RequestType.DELETE.equals(request.getOperationType()))
		{
			localDAO.deleteTest(request.getTest(), context);
			res.setStatus(ResultStatus.OK);
		}
		else if(RequestType.MODIFY.equals(request.getOperationType()))
		{
			localDAO.modifyTest(request.getTest(), context);
			res.setStatus(ResultStatus.OK);
			res.setId(request.getTest().getId());
		}
		
		return res;
	}

}
