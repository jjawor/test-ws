package com.jjawor.elm.ws;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.authorization.WSPermisionLevel;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.dataAccess.UserType;
import com.jjawor.elm.schemas.GetCourseTestListRequest;
import com.jjawor.elm.schemas.GetCourseTestListResponse;
import com.jjawor.elm.schemas.GetCourseUserListRequest;
import com.jjawor.elm.schemas.GetCourseUserListResponse;
import com.jjawor.elm.schemas.GetUserListRequest;
import com.jjawor.elm.schemas.GetUserListResponse;
import com.jjawor.elm.schemas.ResultStatus;

@Endpoint
public class GetCourseTestListEndpoint {
	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@Autowired
	private WSPermisionLevel permisionLevel;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetCourseTestListRequest")
	public @ResponsePayload
	GetCourseTestListResponse handleRequest(
			@RequestPayload GetCourseTestListRequest request) throws Exception {
		GetCourseTestListResponse res=new GetCourseTestListResponse();
		Context context=sessionsHolderBean.getContext(request.getToken());
		if(!permisionLevel.checkPermisionLevel(context, this.getClass())){
			res.setStatus(ResultStatus.NO_SESSION);
			return res;
		}
		if(context.getUser().getType().equals(UserType.PROFESSOR.name())){
		localDAO.getCourseTestList(res.getCourses(),context);
		res.setStatus(ResultStatus.OK);
		}
		else if(context.getUser().getType().equals(UserType.STUDENT.name())){
			localDAO.getStudentCourseTestList(res.getCourses(),context);
			res.setStatus(ResultStatus.OK);
		}
		
		return res;
	}

}
