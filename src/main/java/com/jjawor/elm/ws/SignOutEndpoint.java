package com.jjawor.elm.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.ResultStatus;
import com.jjawor.elm.schemas.SignOutRequest;
import com.jjawor.elm.schemas.SignOutResponse;

@Endpoint
public class SignOutEndpoint {

	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "SignOutRequest")
	public @ResponsePayload
	SignOutResponse handleRequest(
			@RequestPayload SignOutRequest request) throws Exception {
		SignOutResponse response=new SignOutResponse();
		boolean success=sessionsHolderBean.signOut(request.getToken());
		response.setStatus(success?ResultStatus.OK:ResultStatus.FAILURE);

		return response;
	}

}
