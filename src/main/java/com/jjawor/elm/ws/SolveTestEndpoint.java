package com.jjawor.elm.ws;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.authorization.StudentTestData;
import com.jjawor.elm.authorization.WSPermisionLevel;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.Answer;
import com.jjawor.elm.schemas.GetUserListRequest;
import com.jjawor.elm.schemas.GetUserListResponse;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.ResultStatus;
import com.jjawor.elm.schemas.SolveTestRequest;
import com.jjawor.elm.schemas.SolveTestResponse;
import com.jjawor.elm.schemas.Test;

@Endpoint
public class SolveTestEndpoint {
	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@Autowired
	private WSPermisionLevel permisionLevel;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "SolveTestRequest")
	public @ResponsePayload
	SolveTestResponse handleRequest(
			@RequestPayload SolveTestRequest request) throws Exception {
		SolveTestResponse res=new SolveTestResponse();
		Context context=sessionsHolderBean.getContext(request.getToken());
		if(!permisionLevel.checkPermisionLevel(context, this.getClass())){
			Logger.getLogger(this.getClass()).info("authenetication for"+request.getToken()+"failed");
			res.setStatus(ResultStatus.NO_SESSION);
			return res;
		}
		if(context.getStudentTestData()==null)//1.start test
		{//2.create test data
			//check if not solved, if course have test
			if(!localDAO.checkTest(request,context)){
				res.setStatus(ResultStatus.FAILURE);
				res.setMessage("test already solved or test doesn't exists");
				return res;
			}
			List<Question> questions=new ArrayList<Question>();
			Test test=new Test();
			test.setId(request.getTestId());
			//get Categories->get questions->get answers
			localDAO.getPossibleTestQuestionList(questions,test,context);
			context.setStudentTestData(new StudentTestData());
			context.getStudentTestData().setTest(test);
			context.getStudentTestData().setCourseId(request.getCourseId().intValue());
			//get random questions->get random answers
			context.getStudentTestData().setQuestions(this.getTestQuestions(test, questions));
			context.getStudentTestData().setStartTime(new Date());
			Question question=context.getStudentTestData().getNextQuestion();
			if(question==null){
				res.setStatus(ResultStatus.FAILURE);
				res.setMessage("Test has no qustions");
				return res;
			}
			res.getQuestions().add(question);
			res.setStatus(ResultStatus.OK);
		}
		else if(!context.getStudentTestData().getTest().getId().equals(request.getTestId())){
			res.setStatus(ResultStatus.FAILURE);
			res.setMessage("Another test in progress");
		}
		else if(!context.getStudentTestData().isTimeYet()){//end of time
			res.setStatus(ResultStatus.TIME_OUT);
			res.setResult(context.getStudentTestData().getStudentRestult());
			if(!context.getStudentTestData().getTest().isIsElearning())
				this.localDAO.persistStudentResult(res.getResult(),context);
			context.setStudentTestData(null);
		}
		else
		{
			context.getStudentTestData().getAnswers().add(
					request.getStudentAnswers().get(0));
			Question question=context.getStudentTestData().getNextQuestion();
			if(question==null){//end of questions
				//return student result
				res.setResult(context.getStudentTestData().getStudentRestult());
				if(!context.getStudentTestData().getTest().isIsElearning())
					this.localDAO.persistStudentResult(res.getResult(),context);
				context.setStudentTestData(null);
				res.setStatus(ResultStatus.OK);
			}
			else{//next questions - business as usual 
				res.getQuestions().add(question);
				res.setStatus(ResultStatus.OK);
			}
		}
		
		return res;
	}
	private List<Question> getTestQuestions(Test test,List<Question> possibleQuestions){
		List<Question> questions=new ArrayList<Question>();
		Random rand=new Random();
		for(int i=0;i<test.getQuestionsCount().intValue()&&!possibleQuestions.isEmpty();i++){
			int ranomQuestion=rand.nextInt(possibleQuestions.size());
			Question question=possibleQuestions.remove(ranomQuestion);
			List<Answer> possibleAnswers=new ArrayList<Answer>();
			possibleAnswers.addAll(question.getAnswers());
			question.getAnswers().clear();
			//get answers
			List<Answer> choosenAnswer=question.getAnswers();
			List<Answer> trueAnswers=new ArrayList<Answer>();
			List<Answer> falseAnswers=new ArrayList<Answer>();
			for(Answer a:possibleAnswers)
				if(a.isIsTrue())trueAnswers.add(a);
				else falseAnswers.add(a);
			if(trueAnswers.isEmpty())continue;
			int ranomAnswer=rand.nextInt(trueAnswers.size());
			Answer firstAnswer=trueAnswers.remove(ranomAnswer);
			if(test.isAreMultipleAnswers()){//test wielokrotnego wyboru
				possibleAnswers.remove(firstAnswer);
				if(test.getAnswersCount().intValue()>=possibleAnswers.size()+1)
					choosenAnswer.addAll(possibleAnswers);
				else{
					for(int j=1;j<test.getAnswersCount().intValue();j++){
						ranomAnswer=rand.nextInt(possibleAnswers.size());
						choosenAnswer.add(possibleAnswers.remove(ranomAnswer));
					}
				}
			}
			else{//test 1-krotnego wyboru
				if(test.getAnswersCount().intValue()>=falseAnswers.size()+1)
					choosenAnswer.addAll(falseAnswers);
				else{
					for(int j=1;j<test.getAnswersCount().intValue();j++){
						ranomAnswer=rand.nextInt(falseAnswers.size());
						choosenAnswer.add(falseAnswers.remove(ranomAnswer));
					}
						
				}
			}
			ranomAnswer=rand.nextInt(choosenAnswer.size()+1);
			choosenAnswer.add(ranomAnswer,firstAnswer);
			//we add subsequent number to qnswers ids
			for(int k=0;k<question.getAnswers().size();k++)
				question.getAnswers().get(k).setId(BigInteger.valueOf(k));
			questions.add(question);
		}
		System.out.println(questions);
		return questions;
	}
}
