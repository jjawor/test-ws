package com.jjawor.elm.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.authorization.WSPermisionLevel;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.GetStudentResultsListRequest;
import com.jjawor.elm.schemas.GetStudentResultsListResponse;
import com.jjawor.elm.schemas.ResultStatus;

@Endpoint
public class GetStudentResultsListEndpoint {
	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@Autowired
	private WSPermisionLevel permisionLevel;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetStudentResultsListRequest")
	public @ResponsePayload
	GetStudentResultsListResponse handleRequest(
			@RequestPayload GetStudentResultsListRequest request) throws Exception {
		GetStudentResultsListResponse res=new GetStudentResultsListResponse();
		Context context=sessionsHolderBean.getContext(request.getToken());
		if(!permisionLevel.checkPermisionLevel(context, this.getClass())){
			res.setStatus(ResultStatus.NO_SESSION);
			return res;
		}
		localDAO.getStudentResultsList(request,res.getResults(),context);
		res.setStatus(ResultStatus.OK);
		
		return res;
	}

}
