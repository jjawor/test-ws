package com.jjawor.elm.ws;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.authorization.SessionsHolderBean;
import com.jjawor.elm.authorization.WSPermisionLevel;
import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.CreateQuestionRequest;
import com.jjawor.elm.schemas.CreateQuestionResponse;
import com.jjawor.elm.schemas.RequestType;
import com.jjawor.elm.schemas.ResultStatus;

@Endpoint
public class CreateQuestionEndpoint {

	private static final String NAMESPACE_URI = "http://jjawor.com/elm/schemas";
	@Autowired
	private LocalDAO localDAO;
	@Autowired
	private SessionsHolderBean sessionsHolderBean;
	@Autowired
	private WSPermisionLevel permisionLevel;
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "CreateQuestionRequest")
	public @ResponsePayload
	CreateQuestionResponse handleRequest(
			@RequestPayload CreateQuestionRequest request) throws Exception {
		CreateQuestionResponse res = new CreateQuestionResponse();
		Context context=sessionsHolderBean.getContext(request.getToken());
		if(!permisionLevel.checkPermisionLevel(context, this.getClass())){
			res.setStatus(ResultStatus.NO_SESSION);
			return res;
		}
		if(RequestType.CREATE.equals(request.getOperationType()))
		{
			int id=localDAO.createQuestion(request.getQuestion(), context);
			res.setStatus(ResultStatus.OK);
			res.setId(BigInteger.valueOf(id));
		}
		else if(RequestType.DELETE.equals(request.getOperationType()))
		{
			localDAO.deleteQuestion(request.getQuestion(), context);
			res.setStatus(ResultStatus.OK);
		}
		else if(RequestType.MODIFY.equals(request.getOperationType()))
		{
			localDAO.modifyQuestion(request.getQuestion(), context);
			res.setStatus(ResultStatus.OK);
			res.setId(request.getQuestion().getId());
		}
		
		return res;
	}

}
