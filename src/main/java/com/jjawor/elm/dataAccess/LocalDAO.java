package com.jjawor.elm.dataAccess;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.jjawor.elm.authorization.Context;
import com.jjawor.elm.databaseTypes.AnswerHome;
import com.jjawor.elm.databaseTypes.CourseHome;
import com.jjawor.elm.databaseTypes.CourseTest;
import com.jjawor.elm.databaseTypes.CourseTestHome;
import com.jjawor.elm.databaseTypes.Punctation;
import com.jjawor.elm.databaseTypes.PunctationHome;
import com.jjawor.elm.databaseTypes.QuestionHome;
import com.jjawor.elm.databaseTypes.Questioncategory;
import com.jjawor.elm.databaseTypes.QuestioncategoryHome;
import com.jjawor.elm.databaseTypes.QuestioncategoryTest;
import com.jjawor.elm.databaseTypes.QuestioncategoryTestHome;
import com.jjawor.elm.databaseTypes.StudentCourse;
import com.jjawor.elm.databaseTypes.StudentCourseHome;
import com.jjawor.elm.databaseTypes.StudentTest;
import com.jjawor.elm.databaseTypes.StudentTestHome;
import com.jjawor.elm.databaseTypes.TestHome;
import com.jjawor.elm.databaseTypes.UserHome;
import com.jjawor.elm.databaseTypes.Usertype;
import com.jjawor.elm.databaseTypes.UsertypeHome;
import com.jjawor.elm.schemas.Answer;
import com.jjawor.elm.schemas.AssignTestToCourseRequest;
import com.jjawor.elm.schemas.AssignUserToCourseRequest;
import com.jjawor.elm.schemas.Category;
import com.jjawor.elm.schemas.Course;
import com.jjawor.elm.schemas.GetStudentResultsListRequest;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.SolveTestRequest;
import com.jjawor.elm.schemas.StudentResult;
import com.jjawor.elm.schemas.Test;
import com.jjawor.elm.schemas.TestResult;
import com.jjawor.elm.schemas.User;

public class LocalDAO {
	@Autowired
	private UserHome userHome;
	@Autowired
	private UsertypeHome userTypeHome;
	@Autowired
	private CourseHome courseHome;
	@Autowired
	private QuestioncategoryHome questioncategoryHome;
	@Autowired
	private QuestioncategoryTestHome questioncategoryTestHome;
	@Autowired
	private AnswerHome answerHome;
	@Autowired
	private QuestionHome questionHome;
	@Autowired
	private TestHome testHome;
	@Autowired
	private PunctationHome punctationHome;
	@Autowired
	private StudentCourseHome studentCourseHome;
	@Autowired
	private CourseTestHome courseTestHome;
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private StudentTestHome studentTestHome;
	@Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;
	@PersistenceContext
	private EntityManager entityManager;
	Logger logger=Logger.getLogger(LocalDAO.class);
	@Transactional
	public void getUserList(List<User> users){
		Session session =entityManager.unwrap(HibernateEntityManager.class).getSession(); //sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<com.jjawor.elm.databaseTypes.User> ussdb=session.createCriteria(com.jjawor.elm.databaseTypes.User.class).list();
		for(com.jjawor.elm.databaseTypes.User u:ussdb)
			{
			User user=new User();
			user.setName(u.getName());
			user.setLogin(u.getLogin());
			user.setType(u.getUsertype().getName());
			user.setIndexNumber(u.getIndexNumber());
			user.setId(BigInteger.valueOf(u.getId()));
			users.add(user);
			}
	}
	@Transactional
	public void getCourseUserList(List<Course> courses,Context context) {
		Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
		@SuppressWarnings("unchecked")
		List<com.jjawor.elm.databaseTypes.Course> codb=session
		.createCriteria(com.jjawor.elm.databaseTypes.Course.class)
		.add(Restrictions.sqlRestriction("masterUser="+context.getUser().getId())).list();
		for(com.jjawor.elm.databaseTypes.Course c:codb)
		{
			Course course=new Course();
			course.setId(BigInteger.valueOf(c.getId()));
			course.setName(c.getName());
			List<BigInteger> students=course.getStudentsIDs();
			for(StudentCourse sc:c.getStudentCourses())
				students.add(BigInteger.valueOf(sc.getUser().getId()));
			courses.add(course);
		}
	}
	@Transactional
	public void getStudentResultsList(GetStudentResultsListRequest request, List<TestResult> results, Context context) {
		Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
		@SuppressWarnings("unchecked")
		List<com.jjawor.elm.databaseTypes.StudentTest> codb=session
		.createCriteria(com.jjawor.elm.databaseTypes.StudentTest.class)
		.add(Restrictions.sqlRestriction("test="+request.getTestId()))
		.add(Restrictions.sqlRestriction("course="+request.getCourseId())).list();
		for(StudentTest st:codb)
		{
			TestResult tr=new TestResult();
			tr.setEndDate(st.getFinished());
			tr.setMark(st.getResultMark());
			tr.setPoints(BigInteger.valueOf(st.getResult()));
			tr.setUserId(BigInteger.valueOf(st.getUser().getId()));
			tr.setId(BigInteger.valueOf(st.getId()));
			results.add(tr);
		}
	}
	@Transactional
	public void getCourseTestList(List<Course> courses, Context context) {
		Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
		@SuppressWarnings("unchecked")
		List<com.jjawor.elm.databaseTypes.Course> codb=session
		.createCriteria(com.jjawor.elm.databaseTypes.Course.class)
		.add(Restrictions.sqlRestriction("masterUser="+context.getUser().getId())).list();
		for(com.jjawor.elm.databaseTypes.Course c:codb)
		{
			Course course=new Course();
			course.setId(BigInteger.valueOf(c.getId()));
			course.setName(c.getName());
			List<Test> tests=course.getTests();
			for(CourseTest sc:c.getCourseTests()){
				com.jjawor.elm.databaseTypes.Test tdb=sc.getTest();
				Test test=new Test();
				test.setAreMultipleAnswers(tdb.isAreMultipleAnswers());
				test.setIsElearning(tdb.isIsElearning());
				test.setQuestionsCount(BigInteger.valueOf(tdb.getQuestionsCount()));
				test.setTimeForTest(BigInteger.valueOf(tdb.getTimeForTest()));
				test.setName(tdb.getName());
				test.setAnswersCount(BigInteger.valueOf(tdb.getAnswersCount()));
				test.setId(BigInteger.valueOf(tdb.getId()));
				Punctation pdb=tdb.getPunctation();
				if(pdb!=null){
				com.jjawor.elm.schemas.Punctation punctation=new com.jjawor.elm.schemas.Punctation();
				punctation.setId(BigInteger.valueOf(pdb.getId()));
				punctation.setM3(BigInteger.valueOf(pdb.getM3()));
				punctation.setM35(BigInteger.valueOf(pdb.getM35()));
				punctation.setM4(BigInteger.valueOf(pdb.getM4()));
				punctation.setM45(BigInteger.valueOf(pdb.getM45()));
				punctation.setM5(BigInteger.valueOf(pdb.getM5()));
				test.setPunctation(punctation);
				}
				List<BigInteger> categories=test.getCategoriesIDs();
				for(QuestioncategoryTest qcdb:tdb.getQuestioncategoryTests())
					categories.add(BigInteger.valueOf(qcdb.getQuestioncategory().getId()));
				tests.add(test);
			}
			courses.add(course);
		}
	}
	@Transactional
	public void getStudentCourseTestList(List<Course> courses, Context context) {
		Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
		@SuppressWarnings("unchecked")
		List<com.jjawor.elm.databaseTypes.StudentCourse> scodb=session
				.createCriteria(com.jjawor.elm.databaseTypes.StudentCourse.class)
				.add(Restrictions.sqlRestriction("student="+context.getUser().getId())).list();
		@SuppressWarnings("unchecked")
		List<com.jjawor.elm.databaseTypes.StudentTest> finishedTestsdb=session
				.createCriteria(com.jjawor.elm.databaseTypes.StudentTest.class)
				.add(Restrictions.sqlRestriction("student="+context.getUser().getId())).list();
		//blad jezeli test bedzie w wiecej niz jednym kursie
		
		List<Integer> finishedTests=new ArrayList<Integer>();
		for(StudentTest st:finishedTestsdb)
			finishedTests.add(st.getTest().getId());
		for(com.jjawor.elm.databaseTypes.StudentCourse c:scodb)
		{
			com.jjawor.elm.databaseTypes.Course coursedb=c.getCourse();
			Course course=new Course();
			course.setId(BigInteger.valueOf(c.getCourse().getId()));
			course.setName(coursedb.getName());
			List<Test> tests=course.getTests();
			for(CourseTest sc:coursedb.getCourseTests()){
				com.jjawor.elm.databaseTypes.Test tdb=sc.getTest();
				if(finishedTests.contains(tdb.getId()))continue;
				Test test=new Test();
				test.setAreMultipleAnswers(tdb.isAreMultipleAnswers());
				test.setIsElearning(tdb.isIsElearning());
				test.setQuestionsCount(BigInteger.valueOf(tdb.getQuestionsCount()));
				test.setTimeForTest(BigInteger.valueOf(tdb.getTimeForTest()));
				test.setName(tdb.getName());
				test.setAnswersCount(BigInteger.valueOf(tdb.getAnswersCount()));
				test.setId(BigInteger.valueOf(tdb.getId()));
				Punctation pdb=tdb.getPunctation();
				if(pdb!=null){
				com.jjawor.elm.schemas.Punctation punctation=new com.jjawor.elm.schemas.Punctation();
				punctation.setId(BigInteger.valueOf(pdb.getId()));
				punctation.setM3(BigInteger.valueOf(pdb.getM3()));
				punctation.setM35(BigInteger.valueOf(pdb.getM35()));
				punctation.setM4(BigInteger.valueOf(pdb.getM4()));
				punctation.setM45(BigInteger.valueOf(pdb.getM45()));
				punctation.setM5(BigInteger.valueOf(pdb.getM5()));
				test.setPunctation(punctation);
				}
				List<BigInteger> categories=test.getCategoriesIDs();
				for(QuestioncategoryTest qcdb:tdb.getQuestioncategoryTests())
					categories.add(BigInteger.valueOf(qcdb.getQuestioncategory().getId()));
				tests.add(test);
			}
			courses.add(course);
		}
		
	}
	@Transactional
	public void getCategoryQuestionList(List<Category> categories, Context context) {
		Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
		@SuppressWarnings("unchecked")
		List<com.jjawor.elm.databaseTypes.Questioncategory> codb=session
		.createCriteria(com.jjawor.elm.databaseTypes.Questioncategory.class)
		.add(Restrictions.sqlRestriction("masterUser="+context.getUser().getId())).list();
		for(com.jjawor.elm.databaseTypes.Questioncategory c:codb)
		{
			Category category=new Category();
			category.setId(BigInteger.valueOf(c.getId()));
			category.setName(c.getName());
			Questioncategory masterCategory=c.getQuestioncategory();
			if(masterCategory!=null)
				category.setMasterId(BigInteger.valueOf(c.getQuestioncategory().getId()));
			List<Question> questions=category.getQuestions();
			for(com.jjawor.elm.databaseTypes.Question sc:c.getQuestions()){
				Question question=new Question();
				question.setId(BigInteger.valueOf(sc.getId()));
				question.setText(sc.getText());
				//question.setCategoryId(BigInteger.valueOf(sc.getQuestioncategory().getId()));///
				List<Answer> answers=question.getAnswers();
				for(com.jjawor.elm.databaseTypes.Answer an:sc.getAnswers())
				{
					Answer answer=new Answer();
					answer.setId(BigInteger.valueOf(an.getId()));
					answer.setExplenationText(an.getExplenationText());
					answer.setText(an.getText());
					//answer.setQuestionId(BigInteger.valueOf(an.getQuestion().getId()));///
					answer.setIsTrue(an.isIsTrue());
					answers.add(answer);
				}
				questions.add(question);
			}
			categories.add(category);
		}
		
	}
	@Transactional
	public void getPossibleTestQuestionList(List<Question> questions,Test test,
			Context context) {
		com.jjawor.elm.databaseTypes.Test tdb=testHome.findById(test.getId().intValue());
		test.setAreMultipleAnswers(tdb.isAreMultipleAnswers());
		test.setIsElearning(tdb.isIsElearning());
		test.setQuestionsCount(BigInteger.valueOf(tdb.getQuestionsCount()));
		test.setTimeForTest(BigInteger.valueOf(tdb.getTimeForTest()));
		test.setName(tdb.getName());
		test.setAnswersCount(BigInteger.valueOf(tdb.getAnswersCount()));
		test.setId(BigInteger.valueOf(tdb.getId()));
		Punctation pdb=tdb.getPunctation();
		if(pdb!=null){
		com.jjawor.elm.schemas.Punctation punctation=new com.jjawor.elm.schemas.Punctation();
		punctation.setId(BigInteger.valueOf(pdb.getId()));
		punctation.setM3(BigInteger.valueOf(pdb.getM3()));
		punctation.setM35(BigInteger.valueOf(pdb.getM35()));
		punctation.setM4(BigInteger.valueOf(pdb.getM4()));
		punctation.setM45(BigInteger.valueOf(pdb.getM45()));
		punctation.setM5(BigInteger.valueOf(pdb.getM5()));
		test.setPunctation(punctation);
		}
		
		for(QuestioncategoryTest qcdb:tdb.getQuestioncategoryTests())
			for(com.jjawor.elm.databaseTypes.Question qdb:qcdb.getQuestioncategory().getQuestions())
			{
				Question question=new Question();
				question.setId(BigInteger.valueOf(qdb.getId()));
				question.setText(qdb.getText());
				List<Answer> answers=question.getAnswers();
				for(com.jjawor.elm.databaseTypes.Answer adb:qdb.getAnswers())
				{
					Answer answer=new Answer();
					answer.setId(BigInteger.valueOf(adb.getId()));
					answer.setIsTrue(adb.isIsTrue());
					answer.setText(adb.getText());
					answer.setExplenationText(adb.getExplenationText());
					answers.add(answer);
				}
				questions.add(question);
			}
	}
	@Transactional
	public int createUser(com.jjawor.elm.schemas.User us){
		com.jjawor.elm.databaseTypes.User user=new com.jjawor.elm.databaseTypes.User();
		user.setName(us.getName());
		user.setLogin(us.getLogin());
		user.setPassword(us.getPassword());
		//System.out.println(UserType.valueOf(us.getType()).ordinal());
		//System.out.println(userTypeHome.findById(UserType.valueOf(us.getType()).ordinal()));
		user.setUsertype(userTypeHome.findById(UserType.valueOf(us.getType()).ordinal()));
		user.setIndexNumber(us.getIndexNumber());
		userHome.persist(user);
		return user.getId();
	}
	@Transactional
	public void deleteUser(User us) {
		userHome.remove(userHome.findById(us.getId().intValue()));
	}
	@Transactional
	public void deleteAnswer(Answer answer,Context context) {
		//check if we have right user
    	if(answerHome.findById(answer.getId().intValue()).getQuestion().getQuestioncategory().getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
		
		
		answerHome.remove(answerHome.findById(answer.getId().intValue()));
	}
	@Transactional
	public void deleteCategory(Category category, Context context) {
		Questioncategory ccc=questioncategoryHome.findById(category.getId().intValue());
		
		
		//check if we have right user
    	if(ccc.getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
		
		//remove category questions
		for(com.jjawor.elm.databaseTypes.Question qu:ccc.getQuestions())
			questionHome.remove(qu);
		//remove sub-categories
		for(Questioncategory ct:ccc.getQuestioncategories())
			questioncategoryHome.remove(ct);
		//remove association with questions
		for(QuestioncategoryTest ct:ccc.getQuestioncategoryTests())
			questioncategoryTestHome.remove(ct);
		
		questioncategoryHome.remove(ccc);
	}
    @Transactional
	public void deleteCourse(Course course,Context context) {
    	com.jjawor.elm.databaseTypes.Course ccc=courseHome.findById(course.getId().intValue());
    	
    	
    	//check if we have right user
    	if(ccc.getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
    	
    	
    	for(StudentCourse sc:ccc.getStudentCourses())
    		studentCourseHome.remove(sc);
    	for(CourseTest ct:ccc.getCourseTests()){
    		courseTestHome.remove(ct);
    		//delete test
    		Test testWsTempWrapper=new Test();testWsTempWrapper.setId(BigInteger.valueOf(ct.getTest().getId()));
    		this.deleteTest(testWsTempWrapper, context);
    	}
    	courseHome.remove(ccc);
	}
    @Transactional
	public void deleteQuestion(Question question, Context context) {
    	com.jjawor.elm.databaseTypes.Question ccc=questionHome.findById(question.getId().intValue());
    	
    	
    	//check if we have right user
    	if(ccc.getQuestioncategory().getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
    	
    	
    	for(com.jjawor.elm.databaseTypes.Answer an:ccc.getAnswers())
    		answerHome.remove(an);
    	questionHome.remove(ccc);
	}
    @Transactional
	public void deleteTest(Test test, Context context) {
com.jjawor.elm.databaseTypes.Test ccc=testHome.findById(test.getId().intValue());
    	
    	
    	//check if we have right user
    	if(ccc.getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
    	
    
    	for(QuestioncategoryTest qt:ccc.getQuestioncategoryTests())
    		questioncategoryTestHome.remove(qt);
    	for(CourseTest ct:ccc.getCourseTests())
    		courseTestHome.remove(ct);
    	///
    	for(StudentTest st:ccc.getStudentTests())
    		studentTestHome.remove(st);
    	///
    	punctationHome.remove(ccc.getPunctation());
    	testHome.remove(ccc);
		
	}
	@Transactional
	public void modifyUser(User us) {
		com.jjawor.elm.databaseTypes.User user=userHome.findById(us.getId().intValue());
		user.setName(us.getName());
		user.setLogin(us.getLogin());
		if(us.getPassword()!=null)user.setPassword(us.getPassword());
		user.setUsertype(userTypeHome.findById(UserType.valueOf(us.getType()).ordinal()));
		user.setIndexNumber(us.getIndexNumber());
		userHome.persist(user);
	}
	@Transactional
	public void modifyAnswer(Answer object,Context context) {
		
		com.jjawor.elm.databaseTypes.Answer dbObject=answerHome.findById(object.getId().intValue());
		

		//check if we have right user
    	if(dbObject.getQuestion().getQuestioncategory().getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
		

    	//check if we are not dropping test to another user course
    	/*if(questioncategoryHome.findById(object.getCategoryId().intValue()).getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");*/
    	//not neeeded becouse we dont chnge answer's question
    	
		
		dbObject.setText(object.getText());
		dbObject.setExplenationText(object.getExplenationText());
		dbObject.setIsTrue(object.isIsTrue());
		//dbObject.setQuestion(questionHome.findById(object.getQuestionId().intValue()));
		answerHome.persist(dbObject);
	}
	@Transactional
	public void modifyCategory(Category object, Context context) {
		com.jjawor.elm.databaseTypes.Questioncategory dbObject=questioncategoryHome.findById(object.getId().intValue());
		
		
		//check if we have right user
    	if(dbObject.getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");

    	//check if we are not dropping test to another user course
    	/*if(questioncategoryHome.findById(obj.F.WRONG.LINE.F.ect.getCategoryId().intValue()).getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");*///dont needed we dont change user
		
		
		
		dbObject.setName(object.getName());
		///dbObject.setUser(userHome.find.F.WRONG.LINE.F.ById(context.getUser().getId().intValue()));
		questioncategoryHome.persist(dbObject);
	}
	@Transactional
	public void modifyTest(Test object, Context context) {
		com.jjawor.elm.databaseTypes.Test dbObject=testHome.findById(object.getId().intValue());
		
		
		//check if we have right user
    	if(dbObject.getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
		
		
		dbObject.setAreMultipleAnswers(object.isAreMultipleAnswers());
		dbObject.setIsElearning(object.isIsElearning());
		dbObject.setQuestionsCount(object.getQuestionsCount().intValue());
		dbObject.setTimeForTest(object.getTimeForTest().intValue());
		dbObject.setName(object.getName());
		dbObject.setAnswersCount(object.getAnswersCount().intValue());
		
		Punctation punctation=dbObject.getPunctation();
		if(object.getPunctation()==null){//remove punctation
			if(punctation!=null)
				punctationHome.remove(punctation);
		}
		else if(punctation==null){//create punctation
			int id=createPunctation(object.getPunctation(),context);
			dbObject.setPunctation(punctationHome.findById(id));
		}
		else //modyfy puncation
			modifyPunctation(object.getPunctation(),context);

		//remove all privious
		for(QuestioncategoryTest qt:dbObject.getQuestioncategoryTests())
			questioncategoryTestHome.remove(qt);
		//persist all new
		//Set<QuestioncategoryTest> set=new HashSet<QuestioncategoryTest>();
		for(BigInteger bi:object.getCategoriesIDs())
			questioncategoryTestHome.persist(new QuestioncategoryTest(dbObject, questioncategoryHome.findById(bi.intValue())));
		//dbObject.setQuestioncategoryTests(set);
		testHome.persist(dbObject);
		
		
	}

	@Transactional
	public int modifyPunctation(com.jjawor.elm.schemas.Punctation object, Context context) {
		com.jjawor.elm.databaseTypes.Punctation dbObject=punctationHome.findById(object.getId().intValue());
		dbObject.setM3(object.getM3().intValue());
		dbObject.setM35(object.getM35().intValue());
		dbObject.setM4(object.getM4().intValue());
		dbObject.setM45(object.getM45().intValue());
		dbObject.setM5(object.getM5().intValue());
		punctationHome.persist(dbObject);
		return dbObject.getId();
	}
	@Transactional
	public int modifyQuestion(com.jjawor.elm.schemas.Question object, Context context) {

		com.jjawor.elm.databaseTypes.Question dbObject=questionHome.findById(object.getId().intValue());
		
		
		//check if we have right user
    	if(dbObject.getQuestioncategory().getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
		//check if we are not dropping question to another user category
    	if(questioncategoryHome.findById(object.getCategoryId().intValue()).getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
		
		dbObject.setQuestioncategory(questioncategoryHome.findById(object.getCategoryId().intValue()));
		dbObject.setText(object.getText());
		questionHome.persist(dbObject);
		//create associated answers
		Set<com.jjawor.elm.databaseTypes.Answer> oldAnswers=dbObject.getAnswers();
		for(com.jjawor.elm.schemas.Answer an:object.getAnswers()){
			com.jjawor.elm.databaseTypes.Answer oldAnswerSurogate=new com.jjawor.elm.databaseTypes.Answer();
			oldAnswerSurogate.setId(an.getId().intValue());
			if(oldAnswers.contains(oldAnswerSurogate)){
				oldAnswers.remove(oldAnswerSurogate);
				this.modifyAnswer(an, context);
			}
			else this.createAnswer(an, context);
		}
		for(com.jjawor.elm.databaseTypes.Answer oa:oldAnswers)
			answerHome.remove(oa);
		
		return dbObject.getId();
	}
	@Transactional
	public int createCourse(com.jjawor.elm.schemas.Course object,Context context){
		
		com.jjawor.elm.databaseTypes.Course dbObject=new com.jjawor.elm.databaseTypes.Course();
		dbObject.setName(object.getName());
		dbObject.setUser(userHome.findById(context.getUser().getId().intValue()));
		courseHome.persist(dbObject);
		return dbObject.getId();
	}
	@Transactional
	public int createCategory(com.jjawor.elm.schemas.Category object, Context context) {
		com.jjawor.elm.databaseTypes.Questioncategory dbObject=new com.jjawor.elm.databaseTypes.Questioncategory();
		dbObject.setName(object.getName());
		dbObject.setUser(userHome.findById(context.getUser().getId().intValue()));
		questioncategoryHome.persist(dbObject);
		return dbObject.getId();
	}
	@Transactional
	public int createQuestion(com.jjawor.elm.schemas.Question object, Context context) {

		com.jjawor.elm.databaseTypes.Question dbObject=new com.jjawor.elm.databaseTypes.Question();
		
		
		//check if we have right user
    	if(questioncategoryHome.findById(object.getCategoryId().intValue()).getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
		
		
		dbObject.setQuestioncategory(questioncategoryHome.findById(object.getCategoryId().intValue()));
		dbObject.setText(object.getText());
		//dbObject.setUser(userHome.findById(context.getUser().getId().intValue()));
		questionHome.persist(dbObject);
		//create associated answers
		for(com.jjawor.elm.schemas.Answer an:object.getAnswers()){
			an.setQuestionId(BigInteger.valueOf(dbObject.getId()));
			this.createAnswer(an,context);
		}
		
		return dbObject.getId();
	}
	@Transactional
	public int createTest(com.jjawor.elm.schemas.Test object, Context context) {
		com.jjawor.elm.databaseTypes.Test dbObject=new com.jjawor.elm.databaseTypes.Test();
		dbObject.setAreMultipleAnswers(object.isAreMultipleAnswers());
		dbObject.setIsElearning(object.isIsElearning());
		dbObject.setQuestionsCount(object.getQuestionsCount().intValue());
		dbObject.setTimeForTest(object.getTimeForTest().intValue());
		dbObject.setUser(userHome.findById(context.getUser().getId().intValue()));
		dbObject.setName(object.getName());
		dbObject.setAnswersCount(object.getAnswersCount().intValue());
		if(object.getPunctation()!=null){
			int id=createPunctation(object.getPunctation(),context);
			dbObject.setPunctation(punctationHome.findById(id));
		}
		testHome.persist(dbObject);
		//Set<QuestioncategoryTest> set=new HashSet<QuestioncategoryTest>();
		for(BigInteger bi:object.getCategoriesIDs())
			questioncategoryTestHome.persist(new QuestioncategoryTest(dbObject, questioncategoryHome.findById(bi.intValue())));
		//dbObject.setQuestioncategoryTests(set);
		
		
		
		return dbObject.getId();
	}
	@Transactional
	public int createPunctation(com.jjawor.elm.schemas.Punctation object, Context context) {
		com.jjawor.elm.databaseTypes.Punctation dbObject=new com.jjawor.elm.databaseTypes.Punctation();
		dbObject.setM3(object.getM3().intValue());
		dbObject.setM35(object.getM35().intValue());
		dbObject.setM4(object.getM4().intValue());
		dbObject.setM45(object.getM45().intValue());
		dbObject.setM5(object.getM5().intValue());
		punctationHome.persist(dbObject);
		return dbObject.getId();
	}
	@Transactional
	public int createAnswer(com.jjawor.elm.schemas.Answer object,Context context) {
		//check if we have right user
    	if(questionHome.findById(object.getQuestionId().intValue()).getQuestioncategory().getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
		
		com.jjawor.elm.databaseTypes.Answer dbObject=new com.jjawor.elm.databaseTypes.Answer();
		dbObject.setText(object.getText());
		dbObject.setExplenationText(object.getExplenationText());
		dbObject.setIsTrue(object.isIsTrue());
		dbObject.setQuestion(questionHome.findById(object.getQuestionId().intValue()));
		//dbObject.setUser(userHome.findById(context.getUser().getId().intValue()));
		answerHome.persist(dbObject);
		return dbObject.getId();
	}
	@Transactional 
	public void intitUserTypes(){
		logger.info("Local DAO is beeing inicialized");
		Session session =entityManager.unwrap(HibernateEntityManager.class).getSession(); //sessionFactory.getCurrentSession();
		
		entityManager.createNativeQuery("ALTER TABLE elm.userType AUTO_INCREMENT = 1").executeUpdate();
		//we assume we work on Long
		if(Long.valueOf(0).equals((Number) session.createCriteria(Usertype.class).setProjection(Projections.rowCount()).uniqueResult()))	
			for(UserType ut:UserType.values())
				if(!ut.toString().startsWith("_"))
				userTypeHome.persist(new Usertype(ut.toString()));
				
		}
	@Transactional 
	public void intitFirstUser(){
		Session session =entityManager.unwrap(HibernateEntityManager.class).getSession(); //sessionFactory.getCurrentSession();
		//we assume we work on Long
		if(Long.valueOf(0).equals((Number) session.createCriteria(com.jjawor.elm.databaseTypes.User.class).setProjection(Projections.rowCount()).uniqueResult()))
		{
			logger.info("first user admin:admin is beeing inicialized");
			com.jjawor.elm.schemas.User user=new com.jjawor.elm.schemas.User();
			user.setLogin("admin");
			user.setPassword("2121b633087cddcceb42a56bf176db097a39311e22758c5b42aa0f51b7ba75ab");
			user.setType(UserType.ADMIN.name());
			user.setName("Change password or delete this accont.");
			createUser(user);
		}
	}
	@Transactional 
	public com.jjawor.elm.schemas.User getUserInfo(String login,String password){
		Session session =entityManager.unwrap(HibernateEntityManager.class).getSession(); //sessionFactory.getCurrentSession();
		com.jjawor.elm.databaseTypes.User us=(com.jjawor.elm.databaseTypes.User)
			session.createCriteria(com.jjawor.elm.databaseTypes.User.class)
			.add(Restrictions.and(Restrictions.eq("login",login),
			Restrictions.eq("password",password)))
			.uniqueResult();
		com.jjawor.elm.schemas.User user=null;
		if(us!=null){
		user = new com.jjawor.elm.schemas.User();
		user.setName(us.getName());
		user.setLogin(us.getLogin());
		user.setPassword(us.getPassword());
		user.setType(us.getUsertype().getName());
		user.setIndexNumber(us.getIndexNumber());
		user.setId(BigInteger.valueOf(us.getId()));}
		return user;
	}
    //@PostConstruct
    @SuppressWarnings("unused")
	private void init(){
        TransactionTemplate tmpl = new TransactionTemplate(txManager);
        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
            	 intitUserTypes();
            	 intitFirstUser();
            }
        });
   }
    @Transactional
	public int createAssignUserToCourse(AssignUserToCourseRequest object,
			Context context) {
    	
    	//check if we have right user
    	if(courseHome.findById(object.getCourseID().intValue()).getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
    	
    	Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
    	@SuppressWarnings("unchecked")
    	List<StudentCourse> codb=session.createCriteria(StudentCourse.class)
    			.add(Restrictions.sqlRestriction("student="+object.getUserID()))
    			.add(Restrictions.sqlRestriction("course="+object.getCourseID())).list();
    	if(codb.size()>0)throw new RuntimeException("assignment allready in database");
    	
    	
		com.jjawor.elm.databaseTypes.StudentCourse dbObject=new com.jjawor.elm.databaseTypes.StudentCourse();
		dbObject.setCourse(courseHome.findById(object.getCourseID().intValue()));
		dbObject.setUser(userHome.findById(object.getUserID().intValue()));
		studentCourseHome.persist(dbObject);
		return dbObject.getId();
	}
    @Transactional
	public void deleteAssignUserToCourse(AssignUserToCourseRequest request,
			Context context) {
    	
    	//check if we have right user
    	if(courseHome.findById(request.getCourseID().intValue()).getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
    	
    	Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
    	@SuppressWarnings("unchecked")
		List<StudentCourse> codb=session.createCriteria(StudentCourse.class)
    			.add(Restrictions.sqlRestriction("student="+request.getUserID()))
    			.add(Restrictions.sqlRestriction("course="+request.getCourseID())).list();
    	studentCourseHome.remove(codb.get(0));
	}
    @Transactional
	public int createAssignTestToCourse(AssignTestToCourseRequest object,
			Context context) {
    	Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
    	@SuppressWarnings("unchecked")
		List<CourseTest> codb=session.createCriteria(CourseTest.class)
    			.add(Restrictions.sqlRestriction("test="+object.getTestID()))
    			.add(Restrictions.sqlRestriction("course="+object.getCourseID())).list();
    	if(codb.size()>0)throw new RuntimeException("assignment allready in database");
    	
    	
		com.jjawor.elm.databaseTypes.CourseTest dbObject=new com.jjawor.elm.databaseTypes.CourseTest();
		com.jjawor.elm.databaseTypes.Course course=courseHome.findById(object.getCourseID().intValue());
		com.jjawor.elm.databaseTypes.Test test=testHome.findById(object.getTestID().intValue());
		
		//check if we have right user
		if(course.getUser().getId()!=context.getUser().getId().intValue()
		|| test.getUser().getId()!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
		
		
		dbObject.setCourse(course);
		dbObject.setTest(test);
		courseTestHome.persist(dbObject);
		return dbObject.getId();
	}
    @Transactional
	public void deleteAssignTestToCourse(AssignTestToCourseRequest request,
			Context context) {
    	
    	//check if we have right user
    	if(testHome.findById(request.getTestID().intValue()).getUser().getId()
    			!=context.getUser().getId().intValue())
			throw new RuntimeException("No access right!");
    	
    	
    	Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
    	@SuppressWarnings("unchecked")
		List<CourseTest> codb=session.createCriteria(CourseTest.class)
    			.add(Restrictions.sqlRestriction("test="+request.getTestID()))
    			.add(Restrictions.sqlRestriction("course="+request.getCourseID())).list();
    	courseTestHome.remove(codb.get(0));
	}
    @Transactional
	public void persistStudentResult(StudentResult result, Context context) {
    	StudentTest studentTest=new StudentTest();
    	com.jjawor.elm.databaseTypes.Course course=courseHome.findById(context.getStudentTestData().getCourseId());
    	com.jjawor.elm.databaseTypes.Test test=testHome.findById(context.getStudentTestData().getTest().getId().intValue());
    	com.jjawor.elm.databaseTypes.User user=userHome.findById(context.getUser().getId().intValue());
    	studentTest.setCourse(course);
    	studentTest.setTest(test);
    	studentTest.setResult(result.getPoints().intValue());
    	studentTest.setResultMark(result.getMark());
    	studentTest.setUser(user);
    	studentTest.setFinished(new Date());
    	studentTestHome.persist(studentTest);
	}
    @Transactional
	public boolean checkTest(SolveTestRequest request, Context context) {
    	//check if not solved, if course have test
    	Session session =entityManager.unwrap(HibernateEntityManager.class).getSession();
    	@SuppressWarnings("unchecked")
		List<CourseTest> codb1=session.createCriteria(CourseTest.class)
    			.add(Restrictions.sqlRestriction("course="+request.getCourseId()))
    			.add(Restrictions.sqlRestriction("test="+request.getTestId())).list();
    	if(codb1.size()==0)return false;
    	
    	@SuppressWarnings("unchecked")
		List<StudentTest> codb=session.createCriteria(StudentTest.class)
    			.add(Restrictions.sqlRestriction("student="+context.getUser().getId()))
    			.add(Restrictions.sqlRestriction("course="+request.getCourseId()))
    			.add(Restrictions.sqlRestriction("test="+request.getTestId())).list();
    	if(codb.size()>0)return false;
		return true;
	}
}
