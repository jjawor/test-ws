package com.jjawor.elm;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.jjawor.elm.schemas.CreateQuestionRequest;
import com.jjawor.elm.schemas.CreateQuestionResponse;
import com.jjawor.elm.schemas.ResultStatus;
@Aspect
public class LogExceptions {

	
	  private Logger logger = Logger.getLogger(LogExceptions.class);
	  /**
	   * Called between the throw and the catch
	   */
	  @AfterThrowing(pointcut = "execution(* handleRequest(..))", throwing = "e")
	  public void myLogException(JoinPoint joinPoint, Throwable e) {
		  logger.error("AOP intercepted error", e);
		  /*System.out.println(joinPoint.getThis());
		  System.out.println(joinPoint.getTarget());
		  System.out.println(joinPoint.getArgs());
		  System.out.println(joinPoint.getArgs()[0] instanceof CreateQuestionRequest);*/
		  
	  }
	  /*@Around("execution(* handleRequest(..))")
	    public Object translateToDataAccessException(ProceedingJoinPoint pjp) throws Throwable {
	      try {
	        return pjp.proceed();
	      } catch (Exception e) {
	    	  CreateQuestionResponse res=new CreateQuestionResponse();
	    	  res.setStatus(ResultStatus.FAILURE);
	        return res;
	      }
	    }*/


}
