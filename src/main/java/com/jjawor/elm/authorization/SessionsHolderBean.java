package com.jjawor.elm.authorization;

import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.jjawor.elm.dataAccess.LocalDAO;
import com.jjawor.elm.schemas.User;

public class SessionsHolderBean {
	public static final int TIME_TO_LIVE=60*30;
	@Autowired
	private LocalDAO localDAO;
	private ConcurrentHashMap<String, Context> signedIN=new ConcurrentHashMap<String,Context>();
	/*synchronized*/ public Context signIn(String login,String password){
		Context isLogged=isLoggedIn(login,password);
		if(isLogged!=null){isLogged.touch();return isLogged;}
		
		User user=localDAO.getUserInfo(login,password);
		if(user==null)return null;
		String token=generateToken();
		Context context=new Context();
		context.setUser(user);
		context.setToken(token);
		signedIN.put(token, context);
		return context;
	}
	public Context isLoggedIn(String login,String password){//to speed  this up we could do reverse map
		User user=localDAO.getUserInfo(login,password);
		if(user==null)return null;
		Context context=null;
		Iterator<Entry<String,Context>> it=signedIN.entrySet().iterator();
		while(it.hasNext())
		{
			Entry<String,Context> e=it.next();
			if(e.getValue().getUser().getId().equals(user.getId()))
				{context=e.getValue();break;}
		}
		Logger.getLogger(this.getClass()).info("user: "+login+" already logged in");
		return context;
	}
	/*synchronized*/ public Context getContext(String token){
		if(token==null)return null;
		Context context=signedIN.get(token);
		if(context!=null)context.touch();
		return context;
	}
	/*synchronized*/ public boolean signOut(String token){
		Context context=getContext(token);
		if(context!=null)
			endStartedTests(context);
		return signedIN.remove(token)!=null;
	}
	private String generateToken(){
		SecureRandom sr = new SecureRandom();
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<8;i++)
			sb.append(sr.nextInt()/*(char)(sr.nextInt()%('Z'-'A')+'A')*/);
		return sb.toString();
	}
	
	/*synchronized*/ public void cleanOldSessions(){
		Logger.getLogger(SessionsHolderBean.class).info("cleanOldSessions is running");
		Date expired=new Date(new Date().getTime()-1000*TIME_TO_LIVE);
		Iterator<Entry<String,Context>> it=signedIN.entrySet().iterator();
		while(it.hasNext())
		{
			Entry<String,Context> e=it.next();
			if(e.getValue().getLastActive().before(expired))
			{
				endStartedTests(e.getValue());
				signedIN.remove(e.getKey());
			}
		}
		/*for(Entry<String,Context> e:signedIN.entrySet())
			if(e.getValue().getLastActive().before(expired))
				signedIN.remove(e.getKey());*/
	}
	private void endStartedTests(Context context){
		if(context.getStudentTestData()!=null)
			if(!context.getStudentTestData().getTest().isIsElearning())
				this.localDAO.persistStudentResult(context.getStudentTestData().getStudentRestult(),context);
	}
}
