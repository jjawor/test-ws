package com.jjawor.elm.authorization;

import java.util.HashMap;

import org.apache.log4j.Logger;

public class WSPermisionLevel {
	private HashMap<String,String> permissions;
	public boolean checkPermisionLevel(Context context,Class<?> clazz){
		if(context==null)return false;
		String privliged=permissions.get(clazz.getName());
		Logger.getLogger(this.getClass()).info("privlige leveel needed: "+privliged+" level ovned: "+context.getUser().getType());
		if(privliged.contains(context.getUser().getType()))
			return true;
		return false;
	}
	public HashMap<String,String> getPermissions() {
		return permissions;
	}
	public void setPermissions(HashMap<String,String> permissions) {
		this.permissions = permissions;
	}
}
