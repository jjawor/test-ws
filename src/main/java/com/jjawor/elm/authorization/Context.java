package com.jjawor.elm.authorization;

import java.util.Date;

import com.jjawor.elm.schemas.User;

public class Context {
	private User user;
	private String token;
	private Date lastActive;
	private StudentTestData studentTestData;
	public Context(){
		lastActive=new Date();
	}
	public void touch(){
		lastActive=new Date();
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getLastActive() {
		return lastActive;
	}
	public StudentTestData getStudentTestData() {
		return studentTestData;
	}
	public void setStudentTestData(StudentTestData studentTestData) {
		this.studentTestData = studentTestData;
	}
}
