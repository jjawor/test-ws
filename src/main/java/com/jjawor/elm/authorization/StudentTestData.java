package com.jjawor.elm.authorization;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jjawor.elm.schemas.Answer;
import com.jjawor.elm.schemas.Punctation;
import com.jjawor.elm.schemas.Question;
import com.jjawor.elm.schemas.StudentAnswer;
import com.jjawor.elm.schemas.StudentResult;
import com.jjawor.elm.schemas.Test;

public class StudentTestData {
	private Test test;
	private int courseId;
	private List<Question> questions;
	private int currentQuestion=0;
	private List<StudentAnswer> answers;
	private Date startTime;
	public Test getTest() {
		return test;
	}
	public void setTest(Test test) {
		this.test = test;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> question) {
		this.questions = question;
	}
	public Question getNextQuestion(){
		if(questions.size()>currentQuestion){
			return getStripedQuestion(questions.get(currentQuestion++));
		}
			
		else return null;
	}
	public StudentResult getStudentRestult(){
		StudentResult res=new StudentResult();
		if(this.getTest().isIsElearning()){
		res.getStudentAnswers().addAll(this.getAnswers());
		res.getQuestions().addAll(this.getQuestions());
		}
		res.setPoints(BigInteger.valueOf(this.getPoints(this.getQuestions(),this.getAnswers())));
		if(this.getTest().getPunctation()!=null)
		{
			res.setMark(this.getMark(this.getTest().getPunctation(),res.getPoints().intValue()));	
		}
		return res;
	}
	private int getPoints(List<Question> questions,List<StudentAnswer> answers){
		int points=0;
		for(int i=0;i<answers.size();i++)
		{
			List<Answer> a=questions.get(i).getAnswers();
			List<Boolean> sa=answers.get(i).getAnswers();
			if(a.size()!=sa.size())continue;
			boolean trueAnswer=true;
			for(int j=0;j<a.size();j++)
			{
				if((a.get(j).isIsTrue()^sa.get(j)))
					{trueAnswer=false;break;}
			}
			if(trueAnswer)points++;
		}
		return points;
	}
	private float getMark(Punctation punctation,int points){
		if(points>=punctation.getM5().intValue())return 5.0f;
		else if(points>=punctation.getM45().intValue())return 4.5f;
		else if(points>=punctation.getM4().intValue())return 4.0f;
		else if(points>=punctation.getM35().intValue())return 3.5f;
		else if(points>=punctation.getM3().intValue())return 3.0f;
		else return 2.0f;
	}
	private Question getStripedQuestion(Question q){
		Question result=new Question();
		result.setId(q.getId());
		result.setText(q.getText());
		List<Answer> answers=result.getAnswers();
		for(Answer a:q.getAnswers()){
			Answer answer=new Answer();
			answer.setId(a.getId());
			answer.setText(a.getText());
			answers.add(answer);
		}
		return result;
	}
	public boolean isTimeYet(){
		return startTime.after(new Date(new Date().getTime()-this.getTest().getTimeForTest().longValue()*1000l*60l));
	}
	public List<StudentAnswer> getAnswers() {
		if(answers==null)answers=new ArrayList<StudentAnswer>();
		return answers;
	}
	public void setAnswers(List<StudentAnswer> answers) {
		this.answers = answers;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}
