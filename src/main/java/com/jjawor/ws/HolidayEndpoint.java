package com.jjawor.ws;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.oxm.Unmarshaller;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.jjawor.hibernateTutorial.EventManager;
import com.jjawor.service.HumanResourceService;
import com.mycompany.hr.schemas.HolidayRequest;
import com.mycompany.hr.schemas.HolidayResponse;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.xpath.XPath;

@Endpoint
public class HolidayEndpoint {

	private static final String NAMESPACE_URI = "http://mycompany.com/hr/schemas";

	private XPath startDateExpression;

	private XPath endDateExpression;

	private XPath nameExpression;

	private HumanResourceService humanResourceService;

	private LocalSessionFactoryBean sesionFactory;

	public LocalSessionFactoryBean getSesionFactory() {
		return sesionFactory;
	}

	@Autowired
	public void setSesionFactory(LocalSessionFactoryBean sesionFactory) {
		this.sesionFactory = sesionFactory;
	}

	// private Unmarshaller unmarshaller;

	@Autowired
	public HolidayEndpoint(HumanResourceService humanResourceService)
			throws JDOMException {
		this.humanResourceService = humanResourceService;

		Namespace namespace = Namespace.getNamespace("hr", NAMESPACE_URI);

		startDateExpression = XPath.newInstance("//hr:StartDate");
		startDateExpression.addNamespace(namespace);

		endDateExpression = XPath.newInstance("//hr:EndDate");
		endDateExpression.addNamespace(namespace);

		nameExpression = XPath
				.newInstance("concat(//hr:FirstName,' ',//hr:LastName)");
		nameExpression.addNamespace(namespace);
	}

	/*
	 * public Unmarshaller getUnmarshaller() { return unmarshaller; }
	 * 
	 * @Autowired public void setUnmarshaller(Unmarshaller unmarshaller) {
	 * this.unmarshaller = unmarshaller; }
	 */

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "HolidayRequest")
	public @ResponsePayload
	HolidayResponse handleHolidayRequest(
			@RequestPayload HolidayRequest holidayRequest) throws Exception {
		/*
		 * SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		 * Date startDate =
		 * dateFormat.parse(startDateExpression.valueOf(holidayRequest)); Date
		 * endDate =
		 * dateFormat.parse(endDateExpression.valueOf(holidayRequest)); String
		 * name = nameExpression.valueOf(holidayRequest);
		 * humanResourceService.bookHoliday(startDate, endDate, name);
		 */
		humanResourceService.bookHoliday(holidayRequest.getHoliday()
				.getStartDate(), holidayRequest.getHoliday().getEndDate(),
				holidayRequest.getEmployee().getFirstName());
		// unmarshaller.unmarshal(holidayRequest);
		HolidayResponse hr = new HolidayResponse();
		hr.setStatus("fdfsdfdfs");

		System.out.println("ppp");
		new EventManager().createAndStoreEvent("My Event", new Date(),
				sesionFactory);

		return hr;
		// return new Element("HolidayResponse",Namespace.getNamespace("hr",
		// NAMESPACE_URI)).addContent("Status");
		// return null;
	}

}
